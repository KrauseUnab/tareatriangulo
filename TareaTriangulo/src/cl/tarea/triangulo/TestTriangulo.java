package cl.tarea.triangulo;

import static org.junit.Assert.*;

import org.junit.jupiter.api.Test;

class TestTriangulo {

	//@Test // Esto es muy importante @tets para poder realizar la prueba o no se van a ver no se reflejan
	
	/*
	 * PRUEBA PARA UN TRIANGULO ISOSCELES
	 */
		
		@Test
		public void PruebaIsoseles() {
			Triangulo triangulo = new Triangulo(10,20,20);
			assertEquals("Isosceles",triangulo.TipoTriangulo());
		}
		
		@Test
		public void PruebaIsoseles1() {
			Triangulo triangulo = new Triangulo(10,10,20);
			assertEquals("Isosceles",triangulo.TipoTriangulo());
		}
		@Test
		public void PruebaIsoseles2() {
			Triangulo triangulo = new Triangulo(10,10,10);
			assertEquals("Isosceles",triangulo.TipoTriangulo());
		}
		
		/*
		 * PRUEBA PARA UN TRIANGULO EQUILATERO
		 */
			
		@Test
		public void PruebaEquilatero() {
			Triangulo triangulo = new Triangulo(20,20,20);
			assertEquals("Equilatero",triangulo.TipoTriangulo());
		}
		
		@Test
		public void PruebaEquilatero1() {
			Triangulo triangulo = new Triangulo(10,10,20);
			assertEquals("Isosceles",triangulo.TipoTriangulo());
		}
		
		/*
		 * PRUEBA PARA UN TRIANGULO ESCALENO
		 */
				
		@Test
		public void PruebaEscaleno() {
			Triangulo triangulo = new Triangulo(10,20,30);
			assertEquals("Escaleno",triangulo.TipoTriangulo());
		}
		
		@Test
		public void PruebaEscaleno1() {
			Triangulo triangulo = new Triangulo(10,10,20);
			assertEquals("Escaleno",triangulo.TipoTriangulo());
		}
		@Test
		public void PruebaEscaleno2() {
			Triangulo triangulo = new Triangulo(10,10,10);
			assertEquals("Escaleno",triangulo.TipoTriangulo());
		}

		/*
		 * ERROR
		 */
			
		@Test
		public void ErrorIngreso() {
			Triangulo triangulo = new Triangulo();
			triangulo.setLadoA("a");
			assertEquals(false,triangulo.esTriangululo());
		}
		
		@Test
		public void ErrorIngreso1() {
			Triangulo triangulo = new Triangulo();
			triangulo.setLadoA("a","b",5);
			assertEquals(false,triangulo.esTriangululo());
		}


}
